/***
 * Excerpted from "Grails 2: A Quick-Start Guide",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/dkgrails2 for more book information.
 ***/
package com.tekdays

class Sponsor {
	String name
	String website
	String description
	byte[] logo

	String toString(){
		name
	}

	static hasMany = [sponsorships : Sponsorship]

	static constraints = {
		name blank: false
		website blank: false, url: true
		description nullable: true, maxSize: 5000
		logo nullable: true, maxSize: 1000000
		sponsorships nullable: true
	}
}