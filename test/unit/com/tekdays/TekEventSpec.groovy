/***
 * Excerpted from "Grails 2: A Quick-Start Guide",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/dkgrails2 for more book information.
 ***/
package com.tekdays

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin}
 * for usage instructions
 */

@TestFor(TekEvent)
class TekEventSpec extends Specification {

	def setup() {
	}

	def cleanup() {
	}

	void "test toString"() {
		when: "a tekEvent has a name and a city"
		def tekEvent = new TekEvent(name:'Groovy One',
		city: 'San Francisco',
		organizer: [fullName: 'John Doe']
		as TekUser)

		then: "the toString method will combine them."
		tekEvent.toString() == 'Groovy One, San Francisco'
	}
}